-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2019 at 08:04 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropship_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_options`
--

CREATE TABLE `menu_options` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `show_order` int(11) NOT NULL,
  `ref` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_options`
--

INSERT INTO `menu_options` (`id`, `name`, `link`, `show_order`, `ref`) VALUES
(1, 'User Access', '/UserAccess', 1, 'profile_drop_down'),
(2, 'HelpDesk', '/admin', 3, 'profile_drop_down'),
(3, 'Category Setup', '/CategorySetup', 4, 'profile_drop_down'),
(4, 'Admin Panel', '/admin', 5, 'profile_drop_down'),
(5, 'Edit Product Listing', '/admin', 6, 'profile_drop_down'),
(6, 'Refresh Order', '/admin', 7, 'profile_drop_down'),
(7, 'Query Screen', '/QueryScreen', 2, 'profile_drop_down'),
(8, 'Brand Update', '/brandupdate', 8, 'profile_drop_down'),
(9, 'Accountant', '/accountant', 9, 'profile_drop_down'),
(10, 'Advertisement', '/advertisement', 0, 'profile_drop_down'),
(14, 'Membership admin', '/membership', 0, 'profile_drop_down'),
(15, 'Homepage Setup', '/homepage-setup', 10, 'profile_drop_down'),
(16, 'Training Setup', '/trainsetup', 11, 'profile_drop_down'),
(17, 'Exam Setup', '/examsetup', 12, 'profile_drop_down'),
(18, 'Event Admin', '/events', 13, 'style="float:right;margin-right: -124px;'),
(19, 'Blog Admin', '/public-blog', 14, 'profile_drop_down'),
(22, 'SendEmail', '/sendemailforunread', 11, 'profile_drop_down');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_options`
--
ALTER TABLE `menu_options`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_options`
--
ALTER TABLE `menu_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
