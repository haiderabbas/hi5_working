-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2019 at 07:58 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropship_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_options_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `user_id`, `menu_options_id`) VALUES
(262, 41, 18),
(261, 41, 17),
(260, 41, 16),
(259, 41, 15),
(258, 41, 9),
(257, 41, 8),
(256, 41, 6),
(255, 41, 5),
(148, 5, 9),
(147, 5, 8),
(146, 5, 5),
(145, 5, 4),
(144, 5, 3),
(143, 5, 2),
(142, 5, 7),
(141, 5, 10),
(75, 43, 10),
(76, 43, 1),
(77, 43, 7),
(78, 43, 2),
(79, 43, 3),
(80, 43, 4),
(81, 43, 5),
(82, 43, 6),
(83, 43, 8),
(84, 43, 9),
(254, 41, 4),
(253, 41, 3),
(252, 41, 2),
(251, 41, 7),
(250, 41, 1),
(249, 41, 14),
(248, 41, 10),
(263, 41, 19);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
