-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2019 at 07:53 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropship_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `IsAdmin` tinyint(1) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_id` text COLLATE utf8_unicode_ci,
  `webcam_image` text COLLATE utf8_unicode_ci,
  `onlineStatus` int(11) DEFAULT NULL,
  `utc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `avatar`, `IsAdmin`, `location`, `phone_no`, `paypal_email`, `password`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`, `status`, `photo_id`, `webcam_image`, `onlineStatus`, `utc`) VALUES
(2, 'Bakibillah Sakib', 'sakib192@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$bzWRUgJtWHw1HDrCF/WadOvk5td24f7LoC4VjVez20g2XAFsrDlkS', NULL, NULL, '2018-11-16 12:09:19', '2018-11-16 12:09:19', '1', NULL, NULL, NULL, NULL),
(3, 'Arefa', 'arefa@hi5.com', '1542355689.jpg', 1, NULL, NULL, NULL, '$2y$10$lv1HoFX7pJzsYxjPEuZn9ubov9UMUZLMZlsGLyafdIALrKDPPk6OC', NULL, 'nHOrkk8k5fIXpRK23z9Mg71ZYVrQ5njFBinUFsa1QkzVPSjE1pod7k93mIm1', '2018-11-16 13:45:36', '2018-11-16 14:08:09', '1', NULL, NULL, NULL, NULL),
(4, 'Awais', 'awais@cbsurety.com', 'default.png', 1, NULL, NULL, NULL, '$2y$10$5A7Jtkle1JzHNcpkTXyRTupkTaSQF1kSowVI.YXNy.dLRyj0d1VSa', '2018-11-16 12:04:35', NULL, '2018-11-19 02:22:08', '2018-11-19 02:22:08', '1', NULL, NULL, NULL, NULL),
(5, 'Arefa', 'arefa.akhter.nila@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$jbFstj1yBMTnEQdnEuavW.eZCXpfKVkxN8EcFoCGiS.QmDiO38ojK', NULL, '5BebzgV5EEJqb8SXQKfQKG9bA9ZSw4KdqbwwTYw5SZEEQsqBIbKg03WZIoKy', '2018-11-19 02:23:45', '2019-01-16 23:18:57', '1', NULL, NULL, 1, '480'),
(6, 'Muhammad Kashif', 'aa@aa.com', '1544338965.jpg', NULL, NULL, NULL, NULL, '$2y$10$6uVO5vV8cK0prwGcFq4/n.qL1WdWU9WLnPZ58z.EtWoKv9crCcWaC', NULL, '8r6HqN4RPgqdhSikmgTkqgrGupKmJxrPfxnpW7820iUzv4ZJGVxQbMM6r9vY', '2018-11-19 02:29:31', '2019-02-09 01:02:44', '1', NULL, NULL, 1, '-360'),
(41, 'admin', 'admin@hi5.com', '1548357919.jpg', 1, NULL, NULL, NULL, '$2y$10$6uVO5vV8cK0prwGcFq4/n.qL1WdWU9WLnPZ58z.EtWoKv9crCcWaC', '2018-11-16 12:04:35', '78LqpbiGs1nsflgOHoY5lkBR1LeAeehJDy8Cl7FRvWkKsf6sNXGyfHPcfo5z', '2018-11-16 12:04:35', '2019-02-09 01:52:09', '1', '1547977754.jpg', '1548181284.png', 0, '-300'),
(42, 'Ujjal', 'ujjal@hi5.com', '1542771611.JPG', NULL, NULL, NULL, NULL, '$2y$10$f9fqtlj4ISGjqR68dOFj1etBCDwfl2F9uOtaFBUHCZItk9Ipcmgvq', NULL, 'UKPebRGWkqLzYIbJ4qj8EOQoigcy7vk5AD0UZX2kxQ9bT66Adp8Z7yJzlyt7', '2018-11-21 09:36:16', '2018-11-21 09:40:11', '1', NULL, NULL, NULL, NULL),
(43, 'Hamid Raza', 'badshah.hazor@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$7EsnPOuhgVf2mwUSzPUvd.CFZ6jPLpnqhoGzYBvU78JzNq2C/9ECu', NULL, 'oyACFEx8fkcGxG6CurjyN13n3BwvwiIVEifqslWteXO7vGc8ZYJ8me4JY1wm', '2018-12-03 01:09:52', '2018-12-03 01:09:52', '1', NULL, NULL, NULL, NULL),
(44, 'waqas767', 'waqas767@yahoo.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$3aM6H6ZFgW3UUGiLQdPEDOb62vh6IaT9J4BcrqzdQilzAiAyh02x6', NULL, NULL, '2018-12-03 08:59:09', '2018-12-03 08:59:09', '1', NULL, NULL, NULL, NULL),
(45, 'babar afzal', 'babarmalik6444@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$AurO88j5fhBTrvxErZaVj.QOmhHoX7Ft.aqVH6.7qGOLJ79TJx6u2', NULL, NULL, '2018-12-04 05:43:09', '2018-12-04 05:43:09', '1', NULL, NULL, NULL, NULL),
(46, 'Masum Kabir', 'mylearning705@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$9qGpqlt6drufsZ82Qq/9L.oOjXvGLrfmfLNLnxMrxbYeQK/M9tm.G', NULL, 'jkoSnPhe8AGpLqYHzOfunEkictT90Gm8Fk4ulgKQXiRB3Tiu98vweonYn6MX', '2018-12-04 15:11:04', '2018-12-04 15:11:04', '1', NULL, NULL, NULL, NULL),
(47, 'ankur', 'ankurmakavana7@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$E1EH/1KEEO5nZ1FOjE5j4OjlOB/g8SlDR.Ixkp/ELG.ZcruwiyAJ.', NULL, NULL, '2018-12-08 14:51:25', '2018-12-08 14:51:25', '1', NULL, NULL, NULL, NULL),
(48, 'Bakibillah Sakib', 'developer@sakibian.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$vRsVUn0x448z1YLHR5OfHeV5YhTDSnK5IsdtAKbW1.L0y.RnR3JMS', NULL, NULL, '2018-12-24 17:23:27', '2018-12-24 17:23:27', '1', NULL, NULL, NULL, NULL),
(49, 'Saleh', 'unmax.systems@gmail.com', 'default.png', NULL, NULL, NULL, NULL, '$2y$10$bvCSMBiK9FkEq/9GkdRQV.srJSKASmnXmHx68KjJ2/HO72SNHf01G', NULL, NULL, '2018-12-31 13:08:13', '2018-12-31 13:08:13', '1', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
